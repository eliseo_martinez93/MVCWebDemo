﻿using MVCDemo.WebClient.BOL;
using MVCDemo.WebClient.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCDemo.WebClient.Controllers
{
    public class DemoPersonasController : Controller
    {
        #region GETSViews

        // GET: DemoPersonas
        public ActionResult Index()
        {
            return View();
        }


        #endregion

        #region POSTViews

        #endregion



        #region JSonResults
        public ActionResult GetJsonPersonas()
        {
            try
            {
                PersonasBOL _personasBol = new PersonasBOL();
                List<PersonasDTO> _personas = new List<PersonasDTO>();

                _personas = _personasBol.getAll();

                return Json(_personas, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { IsFail = true, Message = ex.Message });
            }
        }
        #endregion

    }
}