﻿using MVCDemo.WebClient.Extensions.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCDemo.WebClient.Controllers
{
    public class DrawerSubMenuController : Controller
    {
        [HttpPost, AjaxOnly]
        public ActionResult GetPartialAutoevaluacionSection()
        {
            try
            {
                return PartialView("GetPartialAutoevaluacionSection");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost, AjaxOnly]
        public ActionResult GetPartialPlanesDeAccionSection()
        {
            try
            {
                return PartialView("GetPartialPLanesDeAccionSection");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost, AjaxOnly]
        public ActionResult GetPartialEstructuraOrganizacionalSection()
        {
            try
            {
                return PartialView("GetPartialEstructuraOrganizacionalSection");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost, AjaxOnly]
        public ActionResult GetPartialActividadDeControlSection()
        {
            try
            {
                return PartialView("GetPartialActividadDeControlSection");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost, AjaxOnly]
        public ActionResult GetPartialPlanesDeAccionConfiguracionSection()
        {
            try
            {
                return PartialView("GetPartialPlanesDeAccionConfiguracionSection");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost, AjaxOnly]
        public ActionResult GetPartialGeneralidadesDelSistemaSection()
        {
            try
            {
                return PartialView("GetPartialGeneralidadesDelSistemaSection");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost, AjaxOnly]
        public ActionResult GetPartialActividadesDeControlConfiguracionSection()
        {
            try
            {
                return PartialView("GetPartialActividadesDeControlCOnfiguracionSection");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost, AjaxOnly]
        public ActionResult GetPartialPlaneamientoProgramacionDelTrabajoSection()
        {
            try
            {
                return PartialView("GetPartialPlaneamientoProgramacionDelTrabajoSection");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost, AjaxOnly]
        public ActionResult GetPartialGestionDeRiesgosContextoSection()
        {
            try
            {
                return PartialView("GetPartialGestionDeRiesgosContextoSection");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost, AjaxOnly]
        public ActionResult GetPartialGestionDeRiesgosIdentificacionSection()
        {
            try
            {
                return PartialView("GetPartialGestionDeRiesgosIdentificacionSection");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost, AjaxOnly]
        public ActionResult GetPartialGestionDeRiesgosValoracionSection()
        {
            try
            {
                return PartialView("GetPartialGestionDeRiesgosValoracionSection");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost, AjaxOnly]
        public ActionResult GetPartialGestionDeRiesgosTratamientoSection()
        {
            try
            {
                return PartialView("GetPartialGestionDeRiesgosTratamientoSection");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost, AjaxOnly]
        public ActionResult GetPartialGestionDeRiesgosMonitoreoSection()
        {
            try
            {
                return PartialView("GetPartialGestionDeRiesgosMonitoreoSection");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost, AjaxOnly]
        public ActionResult GetPartialSPSection()
        {
            try
            {
                return PartialView("GetPartialGestionDeRiesgosMonitoreoSection");
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}