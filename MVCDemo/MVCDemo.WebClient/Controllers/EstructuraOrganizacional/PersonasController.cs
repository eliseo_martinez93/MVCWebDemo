﻿using MVCDemo.WebClient.Extensions.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AGT.GRC.UI.WebClient.Controllers
{
    public class PersonasController : Controller
    {
        // GET: People
        public ActionResult Index()
        {
            return View();
        }

        #region Po-pup Partial View Get section

        /// <summary>
        /// Getting partial view for creating a user
        /// </summary>
        /// <returns></returns>
        [HttpPost, AjaxOnly]
        public ActionResult GetParcialFormAddUser()
        {
            try
            {
                return PartialView("_PartialFormAddUser");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Getting partial view for updating a user
        /// </summary>
        /// <returns></returns>
        [HttpPost, AjaxOnly]
        public ActionResult GetParcialFormUpdateUser()
        {
            try
            {
                return PartialView("_PartialFormUpdateUser");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Getting partial view for view user details
        /// </summary>
        /// <returns></returns>
        [HttpPost, AjaxOnly]
        public ActionResult GetPartialDetailUser()
        {
            try
            {
                return PartialView("_PartialGetDetailUser");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion
    }
}