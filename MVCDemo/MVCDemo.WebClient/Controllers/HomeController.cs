﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCDemo.WebClient.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        // GET: Demo
        public ActionResult Demo()
        {
            return View();
        }

        public ActionResult Error1()
        {
            return View();
        }

        public ActionResult Error2()
        {
            return View();
        }

        public ActionResult DashBoard()
        {
            return View();
        }

        public ActionResult DashBoard2() {
            return View();
        }

        public ActionResult Components()
        {
            return View();
        }

        public ActionResult KendoGrid()
        {
            return View();
        }

        public ActionResult GoogleCharts()
        {
            return View();
        }
        
        public ActionResult JqueryDatatable()
        {
            return View();
        }

        public ActionResult Drawer1()
        {
            return View();
        }

        public ActionResult Drawer2()
        {
            return View();
        }

        public ActionResult ExpandableDrawer()
        {
            return View();
        }

        public ActionResult TextStyles()
        {
            return View();
        }
    }
}