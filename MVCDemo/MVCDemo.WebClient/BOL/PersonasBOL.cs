﻿using MVCDemo.WebClient.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCDemo.WebClient.BOL
{
    public class PersonasBOL
    {
        public List<PersonasDTO> getAll()
        {
            List<PersonasDTO> _personas = new List<PersonasDTO>();

            _personas.Add(new PersonasDTO
            {
                id = 1,
                Nombre1 = "Christian",
                Nombre2 = "Armando",
                Apellido1 = "Gonzalez",
                Apellido2 = "Arevalo",
            });

            _personas.Add(new PersonasDTO
            {
                id = 2,
                Nombre1 = "Juan",
                Nombre2 = "Carlos",
                Apellido1 = "Sosa",
                Apellido2 = "Rivas",
            });

            _personas.Add(new PersonasDTO
            {
                id = 3,
                Nombre1 = "Michael",
                Nombre2 = "Emir",
                Apellido1 = "Reynosa",
                Apellido2 = "Beltran",
            });

            _personas.Add(new PersonasDTO
            {
                id = 4,
                Nombre1 = "Josue",
                Nombre2 = "Eli",
                Apellido1 = "Argueta",
                Apellido2 = "Ayala",
            });

            _personas.Add(new PersonasDTO
            {
                id = 5,
                Nombre1 = "Jorge",
                Nombre2 = "Umberto",
                Apellido1 = "Garay",
                Apellido2 = "De la que phone",
            });

            _personas.Add(new PersonasDTO
            {
                id = 6,
                Nombre1 = "Roberto",
                Nombre2 = "Tadeo",
                Apellido1 = "De la que phone",
                Apellido2 = "Ayala",
            });

            _personas.Add(new PersonasDTO
            {
                id = 7,
                Nombre1 = "Christian",
                Nombre2 = "Armando",
                Apellido1 = "Gonzalez",
                Apellido2 = "Arevalo",
            });

            _personas.Add(new PersonasDTO
            {
                id = 8,
                Nombre1 = "Juan",
                Nombre2 = "Carlos",
                Apellido1 = "Sosa",
                Apellido2 = "Rivas",
            });

            _personas.Add(new PersonasDTO
            {
                id = 9,
                Nombre1 = "Michael",
                Nombre2 = "Emir",
                Apellido1 = "Reynosa",
                Apellido2 = "Beltran",
            });

            _personas.Add(new PersonasDTO
            {
                id = 10,
                Nombre1 = "Josue",
                Nombre2 = "Eli",
                Apellido1 = "Argueta",
                Apellido2 = "Ayala",
            });

            _personas.Add(new PersonasDTO
            {
                id = 11,
                Nombre1 = "Jorge",
                Nombre2 = "Umberto",
                Apellido1 = "Garay",
                Apellido2 = "De la que phone",
            });

            _personas.Add(new PersonasDTO
            {
                id = 12,
                Nombre1 = "Roberto",
                Nombre2 = "Tadeo",
                Apellido1 = "De la que phone",
                Apellido2 = "Ayala",
            });

            _personas.Add(new PersonasDTO
            {
                id = 13,
                Nombre1 = "Christian",
                Nombre2 = "Armando",
                Apellido1 = "Gonzalez",
                Apellido2 = "Arevalo",
            });

            _personas.Add(new PersonasDTO
            {
                id = 14,
                Nombre1 = "Juan",
                Nombre2 = "Carlos",
                Apellido1 = "Sosa",
                Apellido2 = "Rivas",
            });

            _personas.Add(new PersonasDTO
            {
                id = 15,
                Nombre1 = "Michael",
                Nombre2 = "Emir",
                Apellido1 = "Reynosa",
                Apellido2 = "Beltran",
            });

            _personas.Add(new PersonasDTO
            {
                id = 16,
                Nombre1 = "Josue",
                Nombre2 = "Eli",
                Apellido1 = "Argueta",
                Apellido2 = "Ayala",
            });

            _personas.Add(new PersonasDTO
            {
                id = 17,
                Nombre1 = "Jorge",
                Nombre2 = "Umberto",
                Apellido1 = "Garay",
                Apellido2 = "De la que phone",
            });

            _personas.Add(new PersonasDTO
            {
                id = 18,
                Nombre1 = "Roberto",
                Nombre2 = "Tadeo",
                Apellido1 = "De la que phone",
                Apellido2 = "Ayala",
            });

            _personas.Add(new PersonasDTO
            {
                id = 19,
                Nombre1 = "Christian",
                Nombre2 = "Armando",
                Apellido1 = "Gonzalez",
                Apellido2 = "Arevalo",
            });

            _personas.Add(new PersonasDTO
            {
                id = 20,
                Nombre1 = "Juan",
                Nombre2 = "Carlos",
                Apellido1 = "Sosa",
                Apellido2 = "Rivas",
            });

            _personas.Add(new PersonasDTO
            {
                id = 21,
                Nombre1 = "Michael",
                Nombre2 = "Emir",
                Apellido1 = "Reynosa",
                Apellido2 = "Beltran",
            });

            _personas.Add(new PersonasDTO
            {
                id = 22,
                Nombre1 = "Josue",
                Nombre2 = "Eli",
                Apellido1 = "Argueta",
                Apellido2 = "Ayala",
            });

            _personas.Add(new PersonasDTO
            {
                id = 23,
                Nombre1 = "Jorge",
                Nombre2 = "Umberto",
                Apellido1 = "Garay",
                Apellido2 = "De la que phone",
            });

            _personas.Add(new PersonasDTO
            {
                id = 24,
                Nombre1 = "Roberto",
                Nombre2 = "Tadeo",
                Apellido1 = "De la que phone",
                Apellido2 = "Ayala",
            });

            return _personas;
        }
    }
}