﻿function showSnackBar(optionsMessage) {
    var btnSnackBar = $('#snackbar-btn');
    var snackbar = document.querySelector('#snackbar-msj');

    if (optionsMessage.success) {
        $(snackbar).css('background-color', '#33791C');
    }
    else if (optionsMessage.success == false) {
        $(snackbar).css('background-color', '#FF1714');
    }
    else {
        $(snackbar).css('background-color', '#323232');
    }

    var msj = {
        message: optionsMessage.message,
        actionHandler: optionsMessage.actionButton,
        actionText: optionsMessage.nameButton,
        timeout: optionsMessage.timeout
    };
    snackbar.MaterialSnackbar.showSnackbar(msj);
}