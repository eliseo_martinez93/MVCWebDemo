﻿$("#show_notification_drawer").click(function () {
    var button = $("#show_notification_drawer");
    btnValue = $(button).val();
    console.log(btnValue)
    if (btnValue === 'close') {
        $(button).val('open');
        //button.addClass("traslate-left");
        //button.removeClass("traslate-right");
        //showing right drawer
        showRightDrawer(btnValue);
    }
    else {
        $(button).val('close');
        //button.addClass("traslate-right");
        //button.removeClass("traslate-left");

        //hidding right drawer
        showRightDrawer(btnValue);
    }
});


function showRightDrawer(status) {
    var rightDrawer = $('.mdl-right-drawer-layout');
    if (status === 'close') {
        rightDrawer.addClass("show-drawer");
        rightDrawer.removeClass("hide-drawer");
    }
    else {
        rightDrawer.addClass("hide-drawer");
        rightDrawer.removeClass("show-drawer");
    }
}

function showMenuSection(_header) {
    var sectionHeader = $('#'+_header);
    var headerStatus = $('.' + _header);
    var headerBody = $('.' + _header);

    var indicator = headerStatus.val();

    if (indicator == 0) {
        $(sectionHeader).addClass("extended")
        $(sectionHeader).removeClass("compressed");
        headerBody.fadeIn();

        headerStatus.val("1");

    }
    else {
        $(sectionHeader).addClass("compressed")
        $(sectionHeader).removeClass("extended");
        headerStatus.val("0");
        headerBody.fadeOut();
    }
}

$("#txtSearchIncluidos").on('change keyup', function () {
    var _value = $("#txtSearchIncluidos").val().toUpperCase();
    console.log(_value);
    if (this.value.length < 1) {
        $("#reportntable_include_items tr").css("display", "");
    } else {
        $("#reportntable_include_items tr:not(:contains('" + _value + "'))").fadeOut();
        $("#reportntable_include_items tr:contains('" + _value + "')").fadeIn();
    }
});

$("#txtSearchExcluidos").on('change keyup', function () {
    var _value = $("#txtSearchExcluidos").val().toUpperCase();
    console.log(_value);
    if (this.value.length < 1) {
        $("#reportntable_exclude_items tr").css("display", "");
    } else {
        $("#reportntable_exclude_items tr:not(:contains('" + _value + "'))").fadeOut();
        $("#reportntable_include_items tr:contains('" + _value + "')").fadeIn();
    }
});







/* Toggle between adding and removing the "active" and "show" classes when the user clicks on one of the "Section" buttons. The "active" class is used to add a background color to the current button when its belonging panel is open. The "show" class is used to open the specific accordion panel */
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function () {
        this.classList.toggle("active");
        this.nextElementSibling.classList.toggle("show");
    }
}