function showLoading() {
    // remove existing loaders
    $('.loading-container').remove();
    $('<div id="orrsLoader" class="loading-container"><div><div class="mdl-card mdl-shadow--16dp"><h5>Cargando, por favor espere...</h5><div class="mdl-spinner mdl-js-spinner is-active"></div></div></div></div>').appendTo("body");

    componentHandler.upgradeElements($('.mdl-spinner').get());
    setTimeout(function () {
        $('#orrsLoader').css({ opacity: 1 });
    }, 1);
}

function hideLoading() {
    $('#orrsLoader').css({ opacity: 0 });
    setTimeout(function () {
        $('#orrsLoader').remove();
    }, 400);
}

function showDialog(options) {
    options = $.extend({
        id: 'orrsDiag',
        title: null,
        text: null,
        negative: false,
        positive: false,
        cancelable: true,
        contentStyle: null,
        onLoaded: false
    }, options);

    // remove existing dialogs
    $('.dialog-container').remove();
    $(document).unbind("keyup.dialog");

    $('<div id="' + options.id + '" class="dialog-container"><div clas="mdl_cell_dialog_container"><div class="mdl-card mdl-shadow--16dp"></div></div></div>').appendTo("body");
    var dialog = $('#orrsDiag');
    var content = dialog.find('.mdl-card');
    if (options.contentStyle != null) content.css(options.contentStyle);
    if (options.title != null) {
        $('<h5>' + options.title + '</h5>').appendTo(content);
    }
    if (options.text != null) {
        $('<p>' + options.text + '</p>').appendTo(content);
    }
    if (options.negative || options.positive) {
        var buttonBar = $('<div class="mdl-card__actions dialog-button-bar"></div>');
        if (options.negative) {
            options.negative = $.extend({
                id: 'negative',
                title: 'Cancel',
                onClick: function () {
                    return false;
                }
            }, options.negative);
            var negButton = $('<button class="mdl-button mdl-js-button mdl-js-ripple-effect " id="' + options.negative.id + '">' + options.negative.title + '</button>');
            negButton.click(function (e) {
                e.preventDefault();
                if (!options.negative.onClick(e))
                    hideDialog(dialog)
            });
            negButton.appendTo(buttonBar);
        }
        if (options.positive) {
            options.positive = $.extend({
                id: 'positive',
                title: 'OK',
                onClick: function () {
                    return false;
                }
            }, options.positive);
            var posButton = $('<button class="mdl-button mdl-js-button mdl-js-ripple-effect popup_submit_button" id="' + options.positive.id + '">' + options.positive.title + '</button>');
            posButton.click(function (e) {
                e.preventDefault();
                if (!options.positive.onClick(e))
                    hideDialog(dialog)
            });
            posButton.appendTo(buttonBar);
        }
        buttonBar.appendTo(content);
    }
    componentHandler.upgradeDom();
    if (options.cancelable) {
        dialog.click(function () {
            hideDialog(dialog);
        });
        $(document).bind("keyup.dialog", function (e) {
            if (e.which == 27)
                hideDialog(dialog);
        });
        content.click(function (e) {
            e.stopPropagation();
        });
    }
    setTimeout(function () {
        dialog.css({ opacity: 1 });
        if (options.onLoaded)
            options.onLoaded();
    }, 1);
}

function hideDialog(dialog) {
    $(document).unbind("keyup.dialog");
    dialog.css({ opacity: 0 });
    setTimeout(function () {
        dialog.remove();
    }, 400);
}


function showStatusDialog(options) {
    options = $.extend({
        id: 'orrsDiag',
        title: null,
        negative: false,
        positive: false,
        cancelable: true,
        contentStyle: null,
        bodySatus: null,
        onLoaded: false
    }, options);

    // remove existing dialogs
    $('.dialog-container').remove();
    $(document).unbind("keyup.dialog");

    //SETTING BASE POPUP CONTAINER
    $('<div id="' + options.id + '" class="dialog-container"><div clas="mdl_cell_dialog_container"><div class="mdl-card mdl-shadow--16dp"><!--CONTENT GOES HERE--></div></div></div>').appendTo("body");

    var dialog = $('#orrsDiag');
    var content = dialog.find('.mdl-card');

    if (options.contentStyle != null) content.css(options.contentStyle);

    if (options.title != null) {

        $('<h5>' + options.title + '</h5>').appendTo(content);
    }

    if (options.bodySatus != null) {

        $('<div class="StatusContainer">' + options.bodySatus + '</div>').appendTo(content);
    }


    if (options.negative || options.positive) {
        var buttonBar = $('<div class="mdl-card__actions dialog-button-bar"></div>');
        if (options.negative) {
            options.negative = $.extend({
                id: 'negative',
                title: 'Cancel',
                onClick: function () {
                    return false;
                }
            }, options.negative);

            var negButton = $('<button class="mdl-button mdl-js-button mdl-js-ripple-effect " id="' + options.negative.id + '">' + options.negative.title + '</button>');
            negButton.click(function (e) {
                e.preventDefault();
                if (!options.negative.onClick(e))
                    hideDialog(dialog)
            });
            negButton.appendTo(buttonBar);
        }
        if (options.positive) {
            options.positive = $.extend({
                id: 'positive',
                title: 'OK',
                onClick: function () {
                    return false;
                }
            }, options.positive);
            var posButton = $('<button class="mdl-button mdl-js-button mdl-js-ripple-effect popup_submit_button" id="' + options.positive.id + '">' + options.positive.title + '</button>');
            posButton.click(function (e) {
                e.preventDefault();
                if (!options.positive.onClick(e))
                    hideDialog(dialog)
            });
            posButton.appendTo(buttonBar);
        }
        buttonBar.appendTo(content);
    }
    componentHandler.upgradeDom();
    if (options.cancelable) {
        dialog.click(function () {
            hideDialog(dialog);
        });
        $(document).bind("keyup.dialog", function (e) {
            if (e.which == 27)
                hideDialog(dialog);
        });
        content.click(function (e) {
            e.stopPropagation();
        });
    }
    setTimeout(function () {
        dialog.css({ opacity: 1 });
        if (options.onLoaded)
            options.onLoaded();
    }, 1);
}


function showCenteredContentDialog(options) {
    options = $.extend({
        id: 'orrsDiag',
        title: null,
        negative: false,
        positive: false,
        cancelable: true,
        contentStyle: null,
        BodyContent: null,
        AllowComment: false,
        onLoaded: false
    }, options);

    // remove existing dialogs
    $('.dialog-container').remove();
    $(document).unbind("keyup.dialog");

    //SETTING BASE POPUP CONTAINER
    $('<div id="' + options.id + '" class="dialog-container"><div clas="mdl_cell_dialog_container"><div class="mdl-card mdl-shadow--16dp"><!--CONTENT GOES HERE--></div></div></div>').appendTo("body");

    var dialog = $('#orrsDiag');
    var content = dialog.find('.mdl-card');

    if (options.contentStyle != null) content.css(options.contentStyle);

    if (options.title != null) {

        $('<h5>' + options.title + '</h5>').appendTo(content);
    }

    if (options.BodyContent != null) {

        $('<div class="StatusContainer">' + options.BodyContent + '</div>').appendTo(content);
    }

    if (options.AllowComment != false) {
        $('<textarea placeholder="Comentarios" class="mdl-textfield__input" type="text" rows="3" id="pcomment" name="pcomment" style="width:100%"></textarea>').appendTo(content);
        //$('<input class="mdl-textfield__input" type="text" id="pcomment" name="pcomment" placeholder="Comentario" style="width:100%" />').appendTo(content);
    }

    if (options.negative || options.positive) {
        var buttonBar = $('<div class="mdl-card__actions dialog-button-bar"></div>');
        if (options.negative) {
            options.negative = $.extend({
                id: 'negative',
                title: 'Cancel',
                onClick: function () {
                    return false;
                }
            }, options.negative);

            var negButton = $('<button class="mdl-button mdl-js-button mdl-js-ripple-effect " id="' + options.negative.id + '">' + options.negative.title + '</button>');
            negButton.click(function (e) {
                e.preventDefault();
                if (!options.negative.onClick(e))
                    hideDialog(dialog)
            });
            negButton.appendTo(buttonBar);
        }
        if (options.positive) {
            options.positive = $.extend({
                id: 'positive',
                title: 'OK',
                onClick: function () {
                    return false;
                }
            }, options.positive);
            var posButton = $('<button class="mdl-button mdl-js-button mdl-js-ripple-effect popup_submit_button" id="' + options.positive.id + '">' + options.positive.title + '</button>');
            posButton.click(function (e) {
                e.preventDefault();
                if (!options.positive.onClick(e))
                    hideDialog(dialog)
            });
            posButton.appendTo(buttonBar);
        }
        buttonBar.appendTo(content);
    }
    componentHandler.upgradeDom();
    if (options.cancelable) {
        dialog.click(function () {
            hideDialog(dialog);
        });
        $(document).bind("keyup.dialog", function (e) {
            if (e.which == 27)
                hideDialog(dialog);
        });
        content.click(function (e) {
            e.stopPropagation();
        });
    }
    setTimeout(function () {
        dialog.css({ opacity: 1 });
        if (options.onLoaded)
            options.onLoaded();
    }, 1);
}


//popup for dinamic content with cancel and accept buttons
function showDnamicContentDialog(options) {
    options = $.extend({
        id: 'orrsDiag',
        title: null,
        negative: false,
        positive: false,
        cancelable: true,
        contentStyle: null,
        BodyContent: null,
        AllowComment: false,
        onLoaded: false,
        automaticClose: true
    }, options);

    // remove existing dialogs
    $('.dialog-container').remove();
    $(document).unbind("keyup.dialog");

    //SETTING BASE POPUP CONTAINER
    $('<div id="' + options.id + '" class="dialog-container"><div clas="mdl_cell_dialog_container"><div class="mdl-card mdl-shadow--16dp"><!--CONTENT GOES HERE--></div></div></div>').appendTo("body");

    var dialog = $('#orrsDiag');
    var content = dialog.find('.mdl-card');

    if (options.contentStyle != null) content.css(options.contentStyle);

    if (options.title != null) {

        $('<h5>' + options.title + '</h5>').appendTo(content);
    }

    if (options.BodyContent != null) {

        $('<div id="modal-container" class="content-container">' + options.BodyContent + '</div>').appendTo(content);
    }

    if (options.AllowComment != false) {
        $('<textarea placeholder="Comentarios" class="mdl-textfield__input" type="text" rows="3" id="pcomment" name="pcomment" style="width:100%"></textarea>').appendTo(content);
        //$('<input class="mdl-textfield__input" type="text" id="pcomment" name="pcomment" placeholder="Comentario" style="width:100%" />').appendTo(content);
    }

    if (options.negative || options.positive) {
        var buttonBar = $('<div class="mdl-card__actions dialog-button-bar"></div>');
        if (options.negative) {
            options.negative = $.extend({
                id: 'negative',
                title: 'Cancel',
                onClick: function () {
                    return false;
                }
            }, options.negative);

            var negButton = $('<button class="mdl-button mdl-js-button mdl-js-ripple-effect " id="' + options.negative.id + '">' + options.negative.title + '</button>');
            negButton.click(function (e) {
                e.preventDefault();
                if (!options.negative.onClick(e))
                    hideDialog(dialog)
            });
            negButton.appendTo(buttonBar);
        }
        if (options.positive) {
            options.positive = $.extend({
                id: 'positive',
                title: 'OK',
                onClick: function () {
                    return false;
                }
            }, options.positive);
            var posButton = $('<button class="mdl-button mdl-js-button mdl-js-ripple-effect popup_submit_button" id="' + options.positive.id + '">' + options.positive.title + '</button>');
            posButton.click(function (e) {
                e.preventDefault();
                if (!options.positive.onClick(e) && options.automaticClose) {
                    hideDialog(dialog);
                }
            });
            posButton.appendTo(buttonBar);
        }
        buttonBar.appendTo(content);
    }
    componentHandler.upgradeDom();
    if (options.cancelable) {
        dialog.click(function () {
            hideDialog(dialog);
        });
        $(document).bind("keyup.dialog", function (e) {
            if (e.which == 27)
                hideDialog(dialog);
        });
        content.click(function (e) {
            e.stopPropagation();
        });
    }
    setTimeout(function () {
        dialog.css({ opacity: 1 });
        if (options.onLoaded)
            options.onLoaded();
    }, 1);
}