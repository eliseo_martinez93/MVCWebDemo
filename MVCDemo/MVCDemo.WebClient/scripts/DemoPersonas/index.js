﻿$(document).ready(function () {

    $("#gridPersonas").kendoGrid({

        dataSource: {
            transport: {
                read: urlGetJsonPersonas,

            },
            schema: {
                model: {
                    fields: {
                        Nombre1: { type: "string" },
                        Nombre2: { type: "string" },
                        Apellido1: { type: "string" },
                        Apellido2: { type: "string" }
                    }
                }
            },
            pageSize: 20
        },
        height: 550,
        filterable: true,
        sortable: true,
        pageable: true,
        columns: [{
            template: "<input type='checkbox' id='#:id#'/>",
            title: "Seleccionar",
            width: 100
        },
        {
            field: "Nombre1",
            title: "Primer Nombre"
        },
        {
            field: "Nombre2",
            title: "Segundo Nombre"
        },
        {
            field: "Apellido1",
            title: "Primer Apellido"
        },
        {
            field: "Apellido2",
            title: "Segundo Apellido"
        },
        {
            template: "<button id='#:id#' onClick='getDetails(#:id#)' class='mdl-button mdl-js-button mdl-button--icon' ><i class='material-icons'>delete</i></button>"
            +"<button id='#:id#' onClick='getDetails(#:id#)' class='mdl-button mdl-js-button mdl-button--icon' ><i class='material-icons'>mode_edit</i></button>"
            +"<button id='#:id#' onClick='getDetails(#:id#)' class='mdl-button mdl-js-button mdl-button--icon' ><i class='material-icons'>reorder</i></button>",
            title: "Options",
            width: 150
        },
        ]
    });
});