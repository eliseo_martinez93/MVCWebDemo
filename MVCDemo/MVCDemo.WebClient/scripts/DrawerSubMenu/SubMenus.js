﻿function goToAutoEvaluacion() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_1');

    secondaryDrawer.removeClass('hide');
    primaryDrawer.addClass('hide');
}

function goToAutoEvaluacionBack() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_1');

    secondaryDrawer.addClass('hide');
    primaryDrawer.removeClass('hide');
}

function goToPlanAccion() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_2');

    secondaryDrawer.removeClass('hide');
    primaryDrawer.addClass('hide');
}

function goToPlanAccionBack() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_2');

    secondaryDrawer.addClass('hide');
    primaryDrawer.removeClass('hide');
}

function goToEstructuraOrganizacional() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_3');

    secondaryDrawer.removeClass('hide');
    primaryDrawer.addClass('hide');
}

function goToEstructuraOrganizacionalBack() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_3');

    secondaryDrawer.addClass('hide');
    primaryDrawer.removeClass('hide');
}

function goToActividadDeControl() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_4');

    secondaryDrawer.removeClass('hide');
    primaryDrawer.addClass('hide');
}

function goToActividadDeControlBack() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_4');

    secondaryDrawer.addClass('hide');
    primaryDrawer.removeClass('hide');
}

function goToPlanesDeAccionConfiguracion() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_5');

    secondaryDrawer.removeClass('hide');
    primaryDrawer.addClass('hide');
}

function goToPlanesDeAccionConfiguracionBack() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_5');

    secondaryDrawer.addClass('hide');
    primaryDrawer.removeClass('hide');
}

function goToGeneralidadesDelSistema() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_6');

    secondaryDrawer.removeClass('hide');
    primaryDrawer.addClass('hide');
}

function goToGeneralidadesDelSistemaBack() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_6');

    secondaryDrawer.addClass('hide');
    primaryDrawer.removeClass('hide');
}

function goToActividadesDeControlConfiguracion() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_7');

    secondaryDrawer.removeClass('hide');
    primaryDrawer.addClass('hide');
}

function goToActividadesDeControlConfiguracionBack() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_7');

    secondaryDrawer.addClass('hide');
    primaryDrawer.removeClass('hide');
}

function goToPlaneamientoProgramacionDelTrabajo() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_8');

    secondaryDrawer.removeClass('hide');
    primaryDrawer.addClass('hide');
}

function goToPlaneamientoProgramacionDelTrabajoBack() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_8');

    secondaryDrawer.addClass('hide');
    primaryDrawer.removeClass('hide');
}

function goToGestionDeRiesgosContexto() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_9');

    secondaryDrawer.removeClass('hide');
    primaryDrawer.addClass('hide');
}

function goToGestionDeRiesgosContextoBack() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_9');

    secondaryDrawer.addClass('hide');
    primaryDrawer.removeClass('hide');
}

function goToGestionDeRiesgosIdentificacion() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_10');

    secondaryDrawer.removeClass('hide');
    primaryDrawer.addClass('hide');
}

function goToGestionDeRiesgosIdentificacionBack() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_10');

    secondaryDrawer.addClass('hide');
    primaryDrawer.removeClass('hide');
}

function goToGestionDeRiesgosValoracion() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_11');

    secondaryDrawer.removeClass('hide');
    primaryDrawer.addClass('hide');
}

function goToGestionDeRiesgosValoracionBack() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_11');

    secondaryDrawer.addClass('hide');
    primaryDrawer.removeClass('hide');
}

function goToGestionDeRiesgosTratamiento() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_12');

    secondaryDrawer.removeClass('hide');
    primaryDrawer.addClass('hide');
}

function goToGestionDeRiesgosTratamientoBack() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_12');

    secondaryDrawer.addClass('hide');
    primaryDrawer.removeClass('hide');
}

function goToGestionDeRiesgosMonitoreo() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_13');

    secondaryDrawer.removeClass('hide');
    primaryDrawer.addClass('hide');
}

function goToGestionDeRiesgosMonitoreoBack() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_13');

    secondaryDrawer.addClass('hide');
    primaryDrawer.removeClass('hide');
}

function goToComponents() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_0');

    secondaryDrawer.removeClass('hide');
    primaryDrawer.addClass('hide');
}

function goToComponentsBack() {
    var primaryDrawer = $('#primary_drawer_section');
    var secondaryDrawer = $('#secondary_drawer_section_0');

    secondaryDrawer.addClass('hide');
    primaryDrawer.removeClass('hide');
}