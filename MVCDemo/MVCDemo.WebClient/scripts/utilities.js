﻿function DeleteConfirmation(gridName, urlAction) {

    var selected = [];
    var chks = $('input[data-deleteCk=true]');
    for (i = 0; i < chks.length; i++) {
        if (chks[i].checked)
            selected.push(chks[i].name);
    }
    if (selected && selected.length > 0) {
        var res = confirm('¿Está seguro que desea eliminar el registro?');
        if (res) {
            $.ajax({
                url: urlAction,
                data: { deleteids: selected.join() }
            }).done(function () {
                var grid = $("#" + gridName).data("kendoGrid");
                grid.dataSource.read();
            });
        }
    }
    else
        alert("No hay ningún registro seleccionado");
}