﻿//
//STATUS = WORKING
//invocando la pantalla parcial de creacion de beneficiario
function show_GetParcialFormAddUser() {
    showLoading();
    $.post(urlGetParcialFormAddUser, function (data) {
        if (!data.IsFail) {
            showDnamicContentDialog({
                title: 'AGREGAR USUAIRO',
                BodyContent: data,
                negative: {
                    title: 'CANCELAR'
                },
                positive: {
                    title: 'ACEPTAR',
                }
            });
        }
        else {
            showSnackBar({ message: data.Message, timeOut: 10000, success: true });
        }
    })
    .fail(function (xhr) {
        showSnackBar({ message: 'Un error ha ocurrido: ' + xhr.statusText, timeOut: 10000, success: false });
    })
    .always(function () {
        hideLoading();
    });
}

//
//STATUS = WORKING
//invocando la pantalla parcial de actualizacion de beneficiario
function show_GetParcialFormUpdateUser(callBack) {
    showLoading();
    $.post(urlGetParcialFormUpdateUser, function (data) {
        if (!data.IsFail) {
            showDnamicContentDialog({
                title: 'MODIFICAR USUARIO',
                BodyContent: data,
                negative: {
                    title: 'CANCELAR'
                },
                positive: {
                    title: 'ACEPTAR',
                }
            });
        }
        else {
            showSnackBar({ message: data.Message, timeOut: 10000, success: true });
        }
        if (callBack != null) callBack();
    })
    .fail(function (xhr) {
        showSnackBar({ message: 'Un error ha ocurrido: ' + xhr.statusText, timeOut: 10000, success: false });
    })
    .always(function () {
        hideLoading();
    });
}

//
//STATUS = WORKING
//invocando la pantalla parcial de detalle de beneficiario
function show_GetPartialDetailUser(callBack) {
    showLoading();
    $.post(urlGetPartialDetailUser, function (data) {
        if (!data.IsFail) {
            showDnamicContentDialog({
                title: 'DETALLE USUARIO',
                BodyContent: data,
                negative: {
                    title: 'CANCELAR'
                },
                positive: {
                    title: 'ACEPTAR',
                }
            });
        }
        else {
            showSnackBar({ message: data.Message, timeOut: 10000, success: true });
        }
        if (callBack != null) callBack();
    })
    .fail(function (xhr) {
        showSnackBar({ message: 'Un error ha ocurrido: ' + xhr.statusText, timeOut: 10000, success: false });
    })
    .always(function () {
        hideLoading();
    });
}


function deleteUser(_status) {
    showDialog({
        title: 'Eliminar Persona',
        text: 'Esta Seguro de querer eliminar la persona seleccionada permanentemente.',
        negative: {
            title: 'Cancelar'
        },
        positive: {
            title: 'Aceptar',
        }
    });
}