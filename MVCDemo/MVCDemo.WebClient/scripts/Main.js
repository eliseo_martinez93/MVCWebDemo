﻿function showModalDialog(title, htmlTemplate, onClick) {
    showDnamicContentDialog({
        title: title,
        BodyContent: htmlTemplate,
        automaticClose: false,
        negative: {
            title: 'CANCELAR'
        },
        positive: {
            title: 'ACEPTAR',
            onClick: onClick
        }
    });
}

function showSnackBar(optionsMessage) {
    var btnSnackBar = $('#snackbar-btn');
    var snackbar = document.querySelector('#snackbar-msj');

    if (optionsMessage.success) {
        $(snackbar).css('background-color', '#33791C');
    }
    else {
        $(snackbar).css('background-color', '#FF1714');
    }

    var msj = {
        message: optionsMessage.message,
        timeout: optionsMessage.timeOut,
        actionHandler: optionsMessage.actionButton,
        actionText: optionsMessage.nameButton
    };
    snackbar.MaterialSnackbar.showSnackbar(msj);
}
