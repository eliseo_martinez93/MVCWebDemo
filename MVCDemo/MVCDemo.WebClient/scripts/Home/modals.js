﻿var lorem = 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.';
$('#btn1').click(function () {
    showDialog({
        title: 'Information',
        text: lorem
    })
});
$('#btn2').click(function () {
    showDialog({
        title: 'Action',
        text: lorem,
        negative: {
            title: 'Cancelar'
        },
        positive: {
            title: 'Aceptar',
            onClick: function (e) {
                alert('Accion procesada!');
            }
        }
    });
});
$('#btn3').click(function () {
    showDialog({
        title: 'Not cancelable',
        text: 'Este dialogo solamente puede ser cerrado utilizando uno de los botones.',

        positive: {
            title: 'Aceptar'
        },
        cancelable: false
    });
});
$('#btn4').click(function () {
    showLoading();
    setTimeout(function () {
        hideLoading();
    }, 3000);
});

